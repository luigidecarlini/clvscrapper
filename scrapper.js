import $ from 'cheerio';
import rp from 'request-promise';
import cliProgress from 'cli-progress';
import { Parser } from 'json2csv';
import fs from 'fs';

const mainUrl = 'https://clasificados.lavoz.com.ar/inmuebles/departamentos/alquileres/2-dormitorios?barrio[3]=alta-cordoba&barrio[2]=cofico&barrio[1]=alberdi&barrio[0]=centro&ciudad=cordoba&provincia=cordoba&precio-desde=13000&precio-hasta=23000&moneda=pesos&cantidad-de-dormitorios[1]=1-dormitorio'
const pageNumber = '20';
const progressBar = new cliProgress.SingleBar({}, cliProgress.Presets.legacy);
const fields = ['link', 'direccion', 'precio', 'expensas', 'barrio', 'hab', 'patio', 'balcon', 'terraza', 'descripcion'];
const parser = new Parser({ fields })

function getPageUrls() {
  let res = [];
  res.push(mainUrl);
  for (let i = 2; i <= pageNumber; i++) {
    res.push(`${mainUrl}&page=${i}`);
  }
  return res;
};

function getDataFromRequestUrl(url) {
  return rp(url)
    .then(function (html) {
      return html;
    })
    .catch(function (err) {
      console.log(err)
    });
}

function scrapeSites(urls, processHtml) {
  let progress = 0;
  progressBar.start(urls.length, 0);
  return Promise.all(urls.map((url, i) => {
    return new Promise((resolve, reject) => {
      getDataFromRequestUrl(url)
        .then(function (html) {
          progress += 1;
          progressBar.update(progress);
          resolve(processHtml(html, url));
        })
        .catch(function (err) {
          reject(err);
        })
    })
  }))
}

function extractArticlesUrls(html) {
  const articlesUrls = [];
  const linkNodes = $('div.col.col-12.mx1.md-mx0.md-mr1.bg-white.mb2.line-height-3.card.relative.safari-card > a.text-decoration-none', html);
  linkNodes.each((i, elem) => articlesUrls.push(elem.attribs.href));
  return articlesUrls;
};

function extractArticleInfo(html, link) {
  let direccion = $('div.clearfix.pb2.mt2 div.container.px2.md-px0 div.col.col-12.md-pt2 p.h4.bolder.m0', html).text();
  let precio = $('div.col.md-col-4 div.bg-darken-1.px3.py2 div.flex.flex-baseline div.h2.mt0.main.bolder', html).text();
  let barrio = $('div.clearfix.pb2.mt2 div.container.px2.md-px0 div.col.col-12.md-pt2 p.mt0.h4', html).eq(1).text(); // especificar para que capture solo el barrio
  let expensas = $('div.col.md-col-4 div.bg-darken-1.px3.py2 h3.h4.mt0.main.bolder', html).text();
  let mainAttributesNodes = $('div.flex-auto.nowrap.col-6 div.container.title-1lines div.inline-flex.align-baseline2.col-9', html);
  let hab = mainAttributesNodes.eq(3).filter('div.inline-flex.align-baseline2.col-9').text();
  let descripcion = getDescription(html);
  let patio = descripcion.indexOf('patio') != -1 && !sinFalsePositive('patio', descripcion);
  let balcon = descripcion.indexOf('balcon') != -1 && !sinFalsePositive('balcon', descripcion);
  let terraza = descripcion.indexOf('terraza') != -1 && !sinFalsePositive('terraza', descripcion);
  precio = cleanNotNumbers(precio);
  hab = cleanNotNumbers(hab);

  if (descripcion) {
    if (!expensas) {
      expensas = getExpenses(descripcion);
    } else {
      expensas = cleanNotNumbers(expensas);
    }
  }

  return {
    link,
    direccion,
    precio,
    expensas,
    barrio,
    hab,
    patio,
    balcon,
    terraza,
    descripcion
  }
}

function sinFalsePositive(string, description){
  let falsePositive = false;
  let stringIndex = description.indexOf(string);
  let sinExists = description.slice(stringIndex - 10, stringIndex).indexOf('sin') != -1;
  if(stringIndex && sinExists){
    falsePositive = true;
  }
  return falsePositive;
}

function getExpenses(descripcion) {
  let expenses = '';
  let expensasIndex = descripcion.indexOf('expensas');
  if (expensasIndex != -1) {
    expensasIndex = expensasIndex + 'expensas'.length;
    expenses = descripcion.slice(expensasIndex, expensasIndex + 10);
    expenses = cleanNotNumbers(expenses);
    if (!expenses) {
      if(sinFalsePositive('expensas', descripcion)){
        expenses = '0';
      }else {
        expenses = 'Mencionadas en descipción';
      }
    }
  }
  return expenses;
}

function cleanNotNumbers(string) {
  let onlyNumbers = string.replace(/\D/g, '')
  return onlyNumbers;
}

function getDescription(html) {
  let descripcion = $('div.container.main-wrapper.px3.py0.mx-auto.md-px4.mt1 div.col.md-col-8.pr4 div.container.px1.md-px0.h4', html).text();
  descripcion = descripcion.toLowerCase();
  descripcion = descripcion.replace(/á/gi, "a");
  descripcion = descripcion.replace(/é/gi, "e");
  descripcion = descripcion.replace(/í/gi, "i");
  descripcion = descripcion.replace(/ó/gi, "o");
  descripcion = descripcion.replace(/ú/gi, "u");
  return descripcion;
}


const pageUrls = getPageUrls();
console.log('Iniciando extracción de las urls de cada articulo')
const searchArticlesArrays = await scrapeSites(pageUrls, extractArticlesUrls);
progressBar.stop();
const searchArticlesUrls = searchArticlesArrays.flat();

console.log('Iniciando extracción de información de cada articulo')
const articleInfoArrays = await scrapeSites(searchArticlesUrls, extractArticleInfo);
progressBar.stop();

const csv = parser.parse(articleInfoArrays);

fs.writeFile('dptos.csv', csv, function (err) {
  if (err) throw err;
  console.log('file saved');
});

